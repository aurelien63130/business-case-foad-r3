<?php
namespace App\Entity;

use App\Repository\CategoryRepository;
use App\Repository\ProduitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: CategoryRepository::class)]
#[UniqueEntity('label', message: 'Impossible, Cette catégorie existe déjà !')]
class Category{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;


    #[ORM\Column(name:'label', nullable: false, unique: false)]
    #[Assert\NotNull(message: 'Veuillez saisir un label !')]
    private $label;

    #[ORM\Column(name:'ordre', nullable: false, type: 'integer')]
    #[Assert\NotBlank(message: 'Je l\'affiche quand ta catégorie !')]
    private $order;

    #[ORM\ManyToOne(targetEntity: self::class, inversedBy: 'categoryEnfants', cascade: ['remove'])]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private $categoryParent;

    #[ORM\OneToMany(mappedBy: 'categoryParent', targetEntity: self::class, cascade: ['remove'])]
    private $categoryEnfants;

    #[ORM\OneToMany(targetEntity: Produit::class, mappedBy: 'category')]
    private $products;



    public function __construct()
    {
        $this->categoryEnfants = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    public function getProducts(){
        return $this->products;
    }

    public function addProduct(Produit $produit){
        if(!$this->products->contains($produit)){
            $this->products->add($produit);
        }
    }

    public function removeProduct(Produit $produit){
        if($this->products->contains($produit)){
            $this->products->removeElement($produit);
        }
    }

    public function __toString(){
        if($this->label){
            return $this->label;
        } else {
            return  '';
        }
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param mixed $order
     */
    public function setOrder($order): void
    {
        $this->order = $order;
    }


    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param mixed $label
     */
    public function setLabel($label): void
    {
        $this->label = $label;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    public function getCategoryParent(): ?self
    {
        return $this->categoryParent;
    }

    public function setCategoryParent(?self $categoryParent): self
    {
        $this->categoryParent = $categoryParent;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getCategoryEnfants(): Collection
    {
        return $this->categoryEnfants;
    }

    public function addCategoryEnfant(self $categoryEnfant): self
    {
        if (!$this->categoryEnfants->contains($categoryEnfant)) {
            $this->categoryEnfants[] = $categoryEnfant;
            $categoryEnfant->setCategoryParent($this);
        }

        return $this;
    }

    public function removeCategoryEnfant(self $categoryEnfant): self
    {
        if ($this->categoryEnfants->removeElement($categoryEnfant)) {
            // set the owning side to null (unless already changed)
            if ($categoryEnfant->getCategoryParent() === $this) {
                $categoryEnfant->setCategoryParent(null);
            }
        }

        return $this;
    }



}
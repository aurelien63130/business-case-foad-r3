<?php

namespace App\Controller;

use App\Form\SearchType;
use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SearchController extends AbstractController
{
    #[Route('/search', name: 'search')]
    public function index(Request $request, ProduitRepository $pr): Response
    {
        $timeStart = microtime(true);

        $form = $this->createForm(SearchType::class);
        $form->handleRequest($request);
        $produits = $pr->findAll();
        if($form->isSubmitted() and $form->isValid()){
            $filters = $form->getData();
            $produits = $pr->search($filters);
        }

        $timeEnd = microtime(true);

        return $this->render('search/index.html.twig', [
            'form' => $form->createView(),
            'produits'=> $produits,
            'timeLoad'=> (float) $timeEnd - (float) $timeStart
        ]);
    }
}

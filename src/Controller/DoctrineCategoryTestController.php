<?php
namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/test-doctrine/', name: 'test_doctrine_')]
class DoctrineCategoryTestController extends AbstractController{

    #[Route('add', name: 'add')]
    public function add(EntityManagerInterface $em, Request $request){
        $form = $this->createForm(CategoryType::class, new Category());
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em->persist($form->getData());
            $em->flush();

            return  $this->redirectToRoute('test_doctrine_list');
        }

        return $this->render("test-doctrine/add.html.twig", [
            'formulaireCategory'=> $form->createView()
        ]);
    }


    #[Route('list', name: 'list')]
    public function listCategory(CategoryRepository $cr){
        $categories = $cr->findAll();

        return $this->render("test-doctrine/all.html.twig", ["categories"=> $categories]);
    }

    #[Route('detail-courte/{category}', name: 'detail-long')]
    public function detailLongue(Category $category){
        return $this->render('test-doctrine/detail.html.twig', [
            'objet'=> $category
        ]);
    }

    #[Route('remove/{category}', name: 'remove')]
    public function removeCategory(Category $category, EntityManagerInterface $em){
        $em->remove($category);
        $em->flush();

        return  $this->redirectToRoute('test_doctrine_list');
    }

    #[Route('edit/{category}', name: 'edit')]
    public function editCategory(Category $category, EntityManagerInterface $em, Request $request){

       $form = $this->createForm(CategoryType::class, $category);

       $form->handleRequest($request);

       if($form->isSubmitted() && $form->isValid()){

           $em->flush();

           return  $this->redirectToRoute('test_doctrine_list');
       }

        return $this->render('test-doctrine/edit.html.twig', ['form'=> $form->createView()]);
    }
}
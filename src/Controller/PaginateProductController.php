<?php

namespace App\Controller;

use App\Repository\ProduitRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PaginateProductController extends AbstractController
{
    #[Route('/paginate/product/{currentPage}/{nbResult}', name: 'paginate_product')]
    public function index(ProduitRepository $produitRepository, $currentPage, $nbResult): Response
    {
        $nbProduit = $produitRepository->count([]);

        // Nombre de page pleine
       $nbPage = $nbProduit/$nbResult;

        if($nbProduit%$nbResult != 0){
            $nbPage = (int) ($nbProduit/$nbResult) + 1;
        }


        $produits = $produitRepository->findByPagination($currentPage, $nbResult);

        return $this->render('paginate_product/index.html.twig', [
            'produits' => $produits,
            'nbPage'=> $nbPage,
            'currentPage'=> $currentPage,
            'nbResult'=> $nbResult
        ]);
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CookieController extends AbstractController
{
    #[Route('/cookie', name: 'cookie_authorize')]
    public function index(Request $request): Response
    {
        $response = new Response("Cookie Created");
        $response->headers->setCookie(new Cookie("accepted", true));
        // Ici, on réccupére la valeur de notre cookie Theme
        $request->cookies->get('accepted');
        // Ici on supprime un cookie
        $response->headers->remove("accepted");
        // Equivalent :
        $response->headers->clearCookie("accepted");
        return  $response;
    }

    #[Route('/session', name: 'session')]
    public function session(Request $request): Response
    {
        // Réccupére un objet qui permettra de manipuler notre session
        $session = $request->getSession();

        // Reccupére l'élément qui a la clé panier dans notre session
        $session->get('panier');

        // Supprime tous les éléments de la session
        $session->clear();

        // Permet de supprimer un élément spécifique dans la session
        $session->remove("panier");

        // Permet d'ajouter un élément dans notre session HTTP.
        // Ici l'élément aura la clé "panier" et la valeur "toto"
        $session->set("panier", "toto");

        // Réccupére toutes les clés contenus dans notre session
        $keys = $session->all();
    }
}

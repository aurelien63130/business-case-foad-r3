<?php

namespace App\Controller;

use App\Entity\ProductOrder;
use App\Entity\Produit;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/panier', name: 'panier_')]
class PanierController extends AbstractController
{
    #[Route('/{id}', name: 'add', requirements: ['id' => '\d+'])]
    public function addPanier(Produit $produit, Request $request){

        // Ici je cré un nouvel objet ProductOrder
        // Il permettra d'associer un produit à la quantité souhaitée
        $productOrder = new ProductOrder();
        $productOrder->setProduct($produit);
        $productOrder->setQuantity(1);

        // Je réccupére ma session depuis l'objet Request de symfony
        $session = $request->getSession();

        // Je cré un tableau vide qui représente le panier
        $panier = [];

        // Si j'ai déjà un panier en session, je le réccupére
        if($session->has("panier")){
            $panier = $session->get('panier');
        }

        $exist = false;

        // Vérifie si on a déjà ce produit dans le panier
        foreach ($panier as $productOrderElem){
            if($productOrderElem->getProduct() == $produit){
                $exist = true;
                $productOrderElem->setQuantity($productOrderElem->getQuantity() + 1);
            }
        }

        if(!$exist){

            $panier[] = $productOrder;
        }

        // Je met à jour la session avec le nouveau panier
        $session->set("panier", $panier);

        // Je redirige l'utilisateur vers le panier
        return $this->redirectToRoute('panier_display');
    }

    #[Route('/remove-product/{id}', name: 'remove_product')]
    public function removeProduit(Produit $produit, Request $request){
        $session = $request->getSession();
        $panier = $session->get('panier');

        $delete = null;
        foreach ($panier as $key=>$productOrder){
            if($produit == $productOrder->getProduct()){
               $delete = $key;
            }
        }

        unset($panier[$delete]);

        $session->set('panier', $panier);


        return $this->redirectToRoute('panier_display');
    }

    #[Route('/', name: 'display')]
    public function index(Request $request): Response
    {
        // Réccupérer ma session
        $panier = $request->getSession()->get('panier');

        $prix = 0;

        foreach ($panier as $po){
            $prix += $po->getProduct()->getPrix() * $po->getQuantity();
        }

        // Calculer le prix total

        // Afficher mon panier
        return $this->render('panier/index.html.twig', [
            'panier' => $panier,
            'prix'=> $prix
        ]);
    }

    #[Route('/{operator}/{id}', 'addremoveone')]
    public function incrementPanierProduct(Produit $produit, Request $request, $operator){
        $session = $request->getSession();
        $panier = $session->get('panier');

        foreach ($panier as $po){
            if($po->getProduct() == $produit){
                if($operator == 'plus'){
                    $po->setQuantity($po->getQuantity()+1);
                } elseif ($operator == 'moins'){
                    $po->setQuantity($po->getQuantity()-1);
                }

            }
        }

        $session->set('panier', $panier);

        return $this->redirectToRoute('panier_display');
    }
}

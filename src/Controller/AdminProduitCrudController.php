<?php

namespace App\Controller;

use App\Entity\Image;
use App\Entity\Produit;
use App\Form\ProduitType;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

#[Route('/admin/produit/crud')]
#[IsGranted('ROLE_ADMIN')]
class AdminProduitCrudController extends AbstractController
{
    #[Route('/', name: 'admin_produit_crud_index', methods: ['GET'])]
    public function index(ProduitRepository $produitRepository): Response
    {
        return $this->render('admin/produit_crud/index.html.twig', [
            'produits' => $produitRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'admin_produit_crud_new', methods: ['GET', 'POST'])]
    public function new(Request $request, EntityManagerInterface $entityManager, SluggerInterface $slugger): Response
    {
        $produit = new Produit();
        $form = $this->createForm(ProduitType::class, $produit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            // On réccupére notre photo dans la requête image correspond au nom du champ dans notr formulaire
            $pictureProduct = $form->get('image')->getData();

            // Génération d'un nouveau nom sécurisé et unique
            $originalFilename = pathinfo($pictureProduct->getClientOriginalName(), PATHINFO_FILENAME);
            $safeFilename = $slugger->slug($originalFilename);
            $newFilename = $safeFilename.'-'.uniqid().'.'.$pictureProduct->guessExtension();


            // J'upload le fichier dans le dossier contenu dans services.yaml qui a la clé product_image
            // Je l'upload avec son nouveau nom
                    $pictureProduct->move(
                        $this->getParameter('product_image'),
                        $newFilename
                    );

            // Dans ma BDD, j'ajoute le nom unique du fichier pour le retrouver
            $produit->setImage($newFilename);



            // J'enregisstre mon produit
            $entityManager->persist($produit);
            $entityManager->flush();

            // Je redirige
            return $this->redirectToRoute('admin_produit_crud_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin/produit_crud/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    #[Route('/{id}', name: 'admin_produit_crud_show', methods: ['GET'])]
    public function show(Produit $produit): Response
    {
        return $this->render('admin/produit_crud/show.html.twig', [
            'produit' => $produit,
        ]);
    }

    #[Route('/{id}/edit', name: 'admin_produit_crud_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Produit $produit, EntityManagerInterface $entityManager, SluggerInterface $slugger): Response
    {

        $form = $this->createForm(ProduitType::class, $produit);


        $form->handleRequest($request);



        if ($form->isSubmitted() && $form->isValid()) {

            // On réccupére notre photo dans la requête image correspond au nom du champ dans notr formulaire
            $pictureProduct = $form->get('image')->getData();

            if($pictureProduct){
                // Génération d'un nouveau nom sécurisé et unique
                $originalFilename = pathinfo($pictureProduct->getClientOriginalName(), PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$pictureProduct->guessExtension();


                // J'upload le fichier dans le dossier contenu dans services.yaml qui a la clé product_image
                // Je l'upload avec son nouveau nom
                $pictureProduct->move(
                    $this->getParameter('product_image'),
                    $newFilename
                );

                // Dans ma BDD, j'ajoute le nom unique du fichier pour le retrouver
                $produit->setImage($newFilename);

            }

            $entityManager->flush();

            // Je redirige
            return $this->redirectToRoute('admin_produit_crud_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/produit_crud/edit.html.twig', [
            'produit' => $produit,
            'form' => $form
        ]);
    }

    #[Route('/{id}', name: 'admin_produit_crud_delete', methods: ['POST'])]
    public function delete(Request $request, Produit $produit, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$produit->getId(), $request->request->get('_token'))) {
            $entityManager->remove($produit);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin_produit_crud_index', [], Response::HTTP_SEE_OTHER);
    }

    #[Route('/demo', name: 'admin_produit_crud_demo')]
    public function crudDemo(EntityManagerInterface $manager){
        $object = new Produit();

        // Génére une requête de suppression et l'ajoute
        // dans la pile de requête de doctrine
        $manager->remove($object);

        // Génére une requête d'insertion de notre objet et
        // l'ajoute dans la pile de requête de doctrine
        $manager->persist($object);

        // Lorsque l'on modifie notre objet,
        // Doctrine génére automatiquement une requête d'update
        // et l'ajoute dans la pile de requête de doctrine
        $object->setNom("Nouveau nom");

        // Execute toutes les requêtes en attente
        // dans la pile de requête de doctrine
        $manager->flush();
    }
}

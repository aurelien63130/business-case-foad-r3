<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $category1 = new Category();
        $category1->setLabel("Chien");
        $category1->setOrder(1);
        $this->addReference('Chien', $category1);

        $category2 = new Category();
        $category2->setLabel("Chat");
        $category2->setOrder(2);
        $this->addReference('Chat', $category2);

        $category3 = new Category();
        $category3->setLabel("Alimentation");
        $category3->setOrder(1);
        $category3->setCategoryParent($category1);
        $this->addReference('Alimentation-Chien', $category3);



        $category4 = new Category();
        $category4->setLabel("Alimentation");
        $category4->setOrder(1);
        $category4->setCategoryParent($category2);
        $this->addReference("Alimentation-Chat", $category4);

        $category5 = new Category();
        $category5->setLabel("Pannier");
        $category5->setOrder(1);
        $category5->setCategoryParent($category1);
        $this->addReference('Panier-Chien', $category3);


        $manager->persist($category1);
        $manager->persist($category2);
        $manager->persist($category3);
        $manager->persist($category4);

        $manager->flush();
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Produit;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProduitFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {

        $produit1 = new Produit();
        $produit1->setNom("Royal Canin Sterilised 37 pour chat");
        $produit1->setImage('produit1.jpg');
        $produit1->setCategory($this->getReference('Alimentation-Chat'));
        $produit1->setPrix(4.99);
        $produit1->setDescription('Ravie :"Mon chat les adorent, elles ont un bon rapport quantité et qualité prix.
        Avec le paquet de 10kg je tiens facilement 3 mois et demi avec un seul chat à la maison ! Je recommande"');
        $produit1->setNbStar(5);


        $produit2 = new Produit();
        $produit2->setNom("Panier Cozy Cord pour chien et chat");
        $produit2->setImage('produit2.jpg');
        $produit2->setCategory($this->getReference('Panier-Chien'));
        $produit2->setPrix(15.99);
        $produit2->setDescription('Avec le panier Cozy Cord, nous vous proposons un accessoire de couchage pour chien ultra confortable qui réalise un sans faute en matière de qualité et de design ! Son velours à larges côtes de coloris gris et sa doublure de coloris crème/blanc sont indémodables et s’intégreront dans tous les intérieurs.
Votre chien appréciera la matière moelleuse et rembourrée de ce panier ultra confortable qui invite au repos et à la paresse avec son haut rebord de 8 à 11 cm selon la taille du modèle choisi. Que ce soit après une promenade, une activité intense ou tout simplement pour y passer une bonne nuit de sommeil, le panier Cozy Cord offrira à votre animal une agréable sensation de confort ! Vous apprécierez ses finitions soignées et sa matière facile d’entretien, lavable à 30°C.
');
        $produit2->setNbStar(4);



        $manager->persist($produit1);
        $manager->persist($produit2);

        $manager->flush();
    }


    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}

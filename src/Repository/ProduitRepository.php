<?php

namespace App\Repository;

use App\Entity\Produit;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Produit|null find($id, $lockMode = null, $lockVersion = null)
 * @method Produit|null findOneBy(array $criteria, array $orderBy = null)
 * @method Produit[]    findAll()
 * @method Produit[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProduitRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Produit::class);
    }

    public function findByPagination($currentPage, $nbResult){
        return $this->createQueryBuilder('p')
            ->setMaxResults($nbResult)->setFirstResult(($currentPage*$nbResult)-$nbResult)
            ->getQuery()->getResult();
    }

    public function search($filtres){
        $query = $this->createQueryBuilder('p')->leftJoin('p.category', 'categ');


        if(!is_null($filtres["searchBar"])){
            $query->where('p.nom LIKE :nom')
                ->orWhere('p.description LIKE :nom')
                ->orWhere('categ.label LIKE :nom')
                ->setParameter('nom', '%'.$filtres["searchBar"].'%');
        }

        if(!is_null($filtres["category"])){
            $query->andWhere('categ = :categ')->setParameter('categ', $filtres["category"]);
        }

        if(!empty($filtres["nbStar"])){

            $query->andWhere('p.nbStar IN (:array)')->setParameter('array', $filtres["nbStar"]);
        }

        if(!is_null($filtres["prixMin"])){

            $query->andWhere('p.prix > :prixMin')->setParameter('prixMin', $filtres["prixMin"]);
        }

        if(!is_null($filtres["prixMax"])){

            $query->andWhere('p.prix < :prixMax')->setParameter('prixMax', $filtres['prixMax']);
        }


        return $query->getQuery()->getResult();
    }


    // /**
    //  * @return Produit[] Returns an array of Produit objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Produit
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

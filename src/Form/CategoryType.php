<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('label', TextType::class, [
                'label'=>'Nom de la catégorie',
                'attr'=> [
                    'class'=> 'form-control'
                ]
            ])
            ->add('categoryParent', EntityType::class, [
                'attr'=> [
                    'class'=> 'form-control classe2'
                ],
                'class'=> Category::class,
                "required"=> false,
                'label'=> 'Selectionnez la catégorie parente :'
            ])
            ->add('order', NumberType::class,[
                'label'=> 'Ordre d\'affichage',
                'attr'=> ['class'=> 'form-control']
            ])
            ->add('submit', SubmitType::class, [
                'attr'=> ['class'=> 'btn btn-success']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}
